package com.mr.gof.behavioral.mediator;

public class ServiceLevel {
    private String name;

    public String getName() {
        return name;
    }

    public ServiceLevel(String name) {
        this.name = name;
    }
}
