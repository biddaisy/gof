package com.mr.gof.behavioral.mediator;

public interface PaymentParameter {
    void parameterChanged();
}
