package com.mr.gof.behavioral.mediator;

public class Beneficiary {
    private String name;

    public Beneficiary(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
